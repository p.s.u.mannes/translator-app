import { Link, useHistory } from "react-router-dom";
import { useContext } from "react";
import React from "react";
import { AuthContext } from "../contexts/authContext";
import signLanguage from "../signlanguage-removebg-preview.png";

function NavbarLogged() {
  const history = useHistory();
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);

  return (
    <nav className="navLogged bg-light">
      <div className="brand">
        <img src={signLanguage} alt="logo" className="logo" />
        <Link className="title" to={"/"}>
          Lost in Translation
        </Link>
      </div>

      <div className="userLinks">
        <div className="iconUser">
          <i className="material-icons ">account_circle</i>
          <Link className="links" to={"/profile/" + loggedInUser.user.id}>
            {loggedInUser.user.username}
          </Link>
        </div>

        <Link
          className="links"
          to=""
          onClick={(event) => {
            event.preventDefault();
            // Fazendo processo de Logout
            setLoggedInUser({ user: {} });
            localStorage.removeItem("loggedInUser");
            history.push("/");
          }}
        >
          &nbsp;Logout
        </Link>
      </div>
    </nav>

    // {/* <nav className="navbar navbar-expand-lg navbar-light bg-light">
    //   <div className="container-fluid">
    //     <img src={signLanguage} alt="logo" className="logo" />
    //     Lost in Translation
    //     <div className="collapse navbar-collapse" id="navbarText">
    //       <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
    //       <span className="navUser">
    //         <i className="material-icons ">account_circle</i>
    //         <NavLink className="" to={"/profile/" + loggedInUser.user.id}>
    //           <p className="iconAccount">{loggedInUser.user.username}</p>
    //         </NavLink>
    //         {loggedInUser ? (
    //           <Link
    //             to=""
    //             onClick={(event) => {
    //               event.preventDefault();
    //               // Fazendo processo de Logout
    //               setLoggedInUser({ user: {} });
    //               localStorage.removeItem("loggedInUser");
    //               history.push("/");
    //             }}
    //           >
    //             &nbsp;Logout
    //           </Link>
    //         ) : null}
    //       </span>
    //     </div>
    //   </div>
    // </nav> */}
  );
}

export default NavbarLogged;
