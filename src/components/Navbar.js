import signLanguage from "../signlanguage-removebg-preview.png";

function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-light bg-light">
        <div className="container-fluid">
          <div className="navbar-brand">
            <img src={signLanguage} alt="logo" className="logo" />
            Lost in Translation
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
