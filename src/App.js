import { Router, Switch, Route } from "react-router-dom";
import Login from "./components/Login";
import "./App.css";
import Translate from "./components/Translate";
import React from "react";
import history from "./history";
import { AuthContextComponent } from "./contexts/authContext";
import Profile from "./components/Profile";

class App extends React.Component {
  state = {
    isLoggedIn: false,
  };

  handleLogIn = (isLoggedIn) => {
    this.setState({ isLoggedIn: isLoggedIn });
  };
  render() {
    return (
      <Router history={history}>
        <AuthContextComponent>
          <div className="App">
            {/* Here goes the routes */}
            <Switch>
              <Route exact path="/">
                <Login handleLogIn={this.handleLogIn} history={history} />
              </Route>
              <Route
                exact
                path="/translate/:userId"
                component={Translate}
              ></Route>
              <Route exact path="/profile/:userId" component={Profile}></Route>
            </Switch>
          </div>
        </AuthContextComponent>
      </Router>
    );
  }
}

export default App;
